%global         srcname aexpect
%global         gittar %{srcname}-%{version}.tar.gz

# Selftests are provided but skipped because they use unsupported tooling.
%global         with_tests 0

Name:           python-%{srcname}
Version:        1.7.0
Release:        1
Summary:        Aexpect is a python library to control interactive applications
License:        GPLv2
URL:            https://github.com/avocado-framework/aexpect
Source0:        https://github.com/avocado-framework/aexpect/archive/%{version}/%{gittar}
BuildArch:      noarch
Requires:       python3
BuildRequires:  python3-devel
BuildRequires:  python3-setuptools

%description
Aexpect is a python library used to control interactive applications, very
similar to pexpect. You can use it to control applications such as ssh, scp
sftp, telnet, among others.

%package -n python3-%{srcname}
Summary:        %{summary}

%description -n python3-%{srcname}
Aexpect is a python library used to control interactive applications, very
similar to pexpect. You can use it to control applications such as ssh, scp
sftp, telnet, among others.

%prep
%autosetup -n %{srcname}-%{version}

%build
%py3_build

%install
%py3_install
ln -s aexpect_helper %{buildroot}%{_bindir}/aexpect_helper-%{python3_pkgversion}
ln -s aexpect_helper %{buildroot}%{_bindir}/aexpect_helper-%{python3_version}

%check
%if %{with_tests}
selftests/checkall
%endif

%files -n python%{python3_pkgversion}-%{srcname}
%doc README.rst
%{python3_sitelib}/aexpect/
%{python3_sitelib}/aexpect-%{version}-py%{python3_version}.egg-info
%{_bindir}/aexpect_helper*

%changelog
* Fri Apr 7 2023 caodongxia <caodongxia@h-partners.com> - 1.7.0-1
- Upgrade to 1.7.0

* Mon Feb 01 2021 Zhu Huankai <zhuhuankai1@huawei.com> - 1.6.1-1
- Package init
